# ESP32-Beebotte 
## Description
Sample of MQTT communication with ESP32 and Beebotte.  
Control LED turn on / off with MQTT message.

## Requirement
- Arduino IDE ver.1.8
    - pubsubclient
    - ArduinoJson (**version 5.x**, not 6.x)
- mqtt.beebotte.com.pem (included)

## How to use
1. Set up ESP32 develop environment (https://github.com/esp8266/Arduino).
2. Clone this repositry and open "esp32-beebotte.ino".
3. Open "config.h" and write "wifi-SSID", "wifi-password", "channel_token", "topic".
4. Build and write programs to ESP32.  
If connection success, you can see below result infomation.  
![result](https://gitlab.com/secondpc.sc/esp32-beebotte/raw/images/result.png)
5. Send command to control LED.
```
curl -i -H "Content-Tyapplication/json" -X POST -d '{"data":"led_on"}' http://api.beebotte.com/v1/data/publish/[your channel]/[your resource]?token=[your token]
```
```
curl -i -H "Content-Tyapplication/json" -X POST -d '{"data":"led_off"}' http://api.beebotte.com/v1/data/publish/[your channel]/[your resource]?token=[your token]
```

## Publish memo
```
client.publish(topic, "{\"data\":\"data_here\"}");
```

## Reference
https://github.com/mascii/esp32-beebottle  
https://qiita.com/mascii/items/1db06be0950a47e6c720
https://github.com/esp8266/Arduino