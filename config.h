// Wi-FiアクセスポイントのSSIDとパスワード
const char *ssid = "";
const char *password = "";

// クライアントID（任意）
const char *clientID = "ESP32";

// Beebotteのチャンネルトークン
const char* channelToken = "";

// トピック名（"channel/resource"の形式）
const char* topic = "";
